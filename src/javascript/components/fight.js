import { controls } from '../../constants/controls';

const pressedKeysSet = new Set();

function Fighter (fighter, position) {
  this.fighter = fighter;
  this.currentHealth = fighter.health;
  this.indicator = document.getElementById(`${position}-fighter-indicator`);
  this.canMakeCombo = true;
  
  this.updateHealth = function(damage) {
    if (!damage) {
      return;
    }
    this.currentHealth -= damage;
    if (this.currentHealth < 0) {
      this.currentHealth = 0;
    } 
    const width = this.currentHealth / this.fighter.health * 100;
    this.indicator.style.width = width + "%";
  };

  this.blockCombo = async function(fighter) {
    this.canMakeCombo = false;
    await new Promise((resolve) => setTimeout(resolve, 10000));
    this.canMakeCombo = true;
  };
}

export async function fight(firstFighter, secondFighter) {
  
  const leftFighter = new Fighter(firstFighter, 'left');
  const rightFighter = new Fighter(secondFighter, 'right');
  
  return new Promise((resolve) => {
    
    document.addEventListener('keyup', function(event) {
      pressedKeysSet.delete(event.code);
    });
    
    document.addEventListener('keydown', function(event) {
      pressedKeysSet.add(event.code);
      keyHandler(event.code, leftFighter, rightFighter);
      if (leftFighter.currentHealth <= 0) {
        resolve(secondFighter);
      } 
      if (rightFighter.currentHealth <= 0) {
        resolve(firstFighter);
      }
    });
  });
}

function keyHandler(keycode, leftFighter, rightFighter) {
  if (!pressedKeysSet.has(controls.PlayerOneBlock) && !pressedKeysSet.has(controls.PlayerTwoBlock)) {
    if (keycode == controls.PlayerOneAttack) {
      rightFighter.updateHealth(getDamage(leftFighter, rightFighter));
      return;
    } 
    if (keycode == controls.PlayerTwoAttack) {
      leftFighter.updateHealth(getDamage(rightFighter, leftFighter));
      return;
    }  
  } 
  
  if (leftFighter.canMakeCombo && testForCombo(controls.PlayerOneCriticalHitCombination)) {
    rightFighter.updateHealth(2 * leftFighter.fighter.attack);
    leftFighter.blockCombo();
    return;
  } 
  
  if (rightFighter.canMakeCombo && testForCombo(controls.PlayerTwoCriticalHitCombination)) {
    leftFighter.updateHealth(2 * rightFighter.fighter.attack);
    rightFighter.blockCombo();
  }
}

function testForCombo(codes) {
  for (let code of codes) {
    if (!pressedKeysSet.has(code)) {
      return false;
    }
  }
  return true;
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker.fighter);
  const blockPower = getBlockPower(defender.fighter);
  const damage = hitPower - blockPower;
  return (damage > 0) ? damage : 0;
}

export function getHitPower(fighter) {
  return (Math.random() + 1) * fighter.attack;
}

export function getBlockPower(fighter) {
  return (Math.random() + 1) * fighter.defense;
}
