import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';
import App from '../../app';

export function showWinnerModal(fighter) {
  // call showModal function 
  const fighterImage = createFighterImage(fighter);
  const winner = { 
    title: `Winner is ${fighter.name}`, 
    bodyElement: fighterImage, 
    onClose: restartGame 
  };
  showModal(winner);

}

function restartGame() {
  const root = document.getElementById('root');
  root.innerHTML = '';
  new App();
}