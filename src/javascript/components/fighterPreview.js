import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const { name, health, attack, defense } = fighter;
    const fighterImage = createFighterImage(fighter);
    const nameElement = createElement({ tagName: 'span', className: 'fighter-preview___params' });
    nameElement.innerText = "Name: " + name;
    const healthElement = createElement({ tagName: 'span', className: 'fighter-preview___params' });
    healthElement.innerText = "Health: " + health;
    const attackElement = createElement({ tagName: 'span', className: 'fighter-preview___params' });
    attackElement.innerText = "Attack: " + attack;
    const defenseElement = createElement({ tagName: 'span', className: 'fighter-preview___params' });
    defenseElement.innerText = "Defense: " + defense;
    fighterElement.append(nameElement, fighterImage, attackElement, healthElement, defenseElement);
  }
  
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
